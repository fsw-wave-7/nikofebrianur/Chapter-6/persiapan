'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

   await queryInterface.bulkInsert('Histories', [{
    score: "80",
    roomId: "ABCD",
    winLose: "WIN",
    winRate: "85%",
    lastOnline: "2 minutes ago",
    user_id: "1",
    createdAt: new Date,
    updatedAt: new Date
  }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Histories', null, {});
  }
};
