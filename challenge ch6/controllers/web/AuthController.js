const { Admin } = require('../../models')
const bcrypt = require('bcrypt')

class AuthController {

    registerAdmin = (req,res, next) => {
        Admin.register(req.body)
        .then(() => {
            res.redirect("/dasbor")
        })
        .catch((err) => next(err))
    }

    login = (req, res) => {
        res.render("login", {
            judul: "login",
            content: "./pages/login",
        })
    }

    doLogin = async (req, res) => {
        const body = req.body
        
        if (!(body.username && body.password)) {
            return res.status(400).send({
                error: "Data not formatted properly"
            })
        }

        User.findOne({
            where: {username: body.username}
        })
        .then(user => {
            bcrypt.compare(body.password, user.password, (err, data) => {
                if (err) throw err

                if (data) {
                    res.cookie('loginData', JSON.stringify(user))
                    res.redirect('/')
                } else {
                    return res.status(401).json({ msg: "Invalid credencial"
                    })
                }
            })
        })
        .catch(err => {
            return res.status(401).json({msg: "Invalid credencial"}
            )
        })
    }

    whoami = (req,res) => {
        res.render("index", req.user.dataValues)
    }

    logout = (req, res) => {
        res.clearCookie('loginData')
        res.redirect('/')
    }
}

module.exports = AuthController