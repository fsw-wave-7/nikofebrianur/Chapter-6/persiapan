const { User, Admin, History, Biodata  } = require('../../models')
const bcrypt = require("bcrypt")

class HomeController {    

    //user
    index = (req, res) => {
        User.findAll()
        .then(users => {
            res.render(join(__dirname, '../../views/index'), {
                content: './pages/dasbor',
                users: users
            })
        })
    }

    getHistory = (req, res) => {
        
    }


    getBio = (req, res) => {
        User.findOne({
            where: {
                id: req.params.id
            },        
        })
        .then(users => {
            res.render(join(__dirname, '../../views/index'), {
                content: './pages/userBiodata',
                users: users
            })
        })
    }

    add = (req, res) => {
        res.render(join(__dirname, '../../views/index'), {
            content: './pages/addList'
        })
    }

    saveUser = async (req, res) => {
        const salt = await bcrypt.genSalt(10)

        User.create({
            name: req.body.name,
            username: req.body.username,
            age: req.body.age,
            password: await bcrypt.hash(req.body.password, salt),
            user_biodata: {
                address: req.body.address,
                nik: req.body.nik,
                education: req.body.education,
            }, 
        // },
        //     {
        //         include: {
        //         model: UserBiodata,
        //         as: 'user_biodata',
        //     }
        })
        .then(() => {
            res.redirect('/')
        }).catch(err => {
            console.log(err)
        })
    }

    userEdit = (req, res) => {
        User.findOne({
            where: {
                id: req.params.id
            },
            include: 'user_biodata',
        }).then(user => {
            res.render(join(__dirname, '../../views/index'), {
                content: './pages/userEdit',
                user: user
            })
        })
    }

    updateUser = (req,res) => {
        User.update({
            name: req.body.name,
            username: req.body.username,
            age: req.body.age,
            password: req.body.password
        }, 
            {
            where: { id: req.params.id }
        })

        .then(user => {
            res.redirect('/')
        })  .catch(err => {
            res.status(422).json("Can't update user")
        })
    }

    delete = (req, res) => {
        User.destroy({
            where: {
                id: req.params.id
            }
        })
        .then(() => {
            res.redirect('/')
        })
    }

    //admin
    adminAdd = (req,res) => {
        res.render("index", {
            judul: "Admin Register",
            content: "./pages/addAdmin",
            admin: req.user.dataValues,
        })
    }

    adminList = (req, res) => {
        Admin.findAll().then((admin) => {
            res.render("index", {
                content: "./pages/adminList",
                users: admin,
                admin: req.user.dataValues,
            })
        })
    }

    adminEdit = (req, res) => {

    }

    adminSave = (req,res) => {
        // const salt = await bcrypt.genSalt(10)

        Admin.create({
            name: req.body.name,
            username: req.body.username,
            age: req.body.age,
            password: req.body.password,
            user_biodata: {
                address: req.body.address,
                nik: req.body.nik,
                education: req.body.education,
            }, 
        // },
        //     {
        //         include: {
        //         model: UserBiodata,
        //         as: 'user_biodata',
        //     }
        })
        .then(() => {
            res.redirect('/')
        }).catch(err => {
            console.log(err)
        })
    }

    adminDelete = (req, res) => {

    }
}

module.exports = HomeController
