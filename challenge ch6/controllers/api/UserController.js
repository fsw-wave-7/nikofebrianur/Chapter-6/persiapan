const { User, History } = require('../../models')
const { successResponse } = require("../../helpers/response")

class UserController {
    
    getUser = (req, res) => {
            User.findAll().then((user) => {
                res.send(user);
            });    
    }

    getDetailUser = (req, res) => {
        User.findOne({
			where: {id: req.params.id},
			include: [
				{
					model: History,
					as: "history",
				},
			],
		}).then((user) => {
			res.send(user);
		})
    }
   
    createRoom = (req, res) => {
		const room_id = req.body.roomId
		const user_id = req.body.user_id
		History.create({
			room_id,
			user_id,
		}).then((data) => {
			successResponse(res, 201, data)
		})
	}

    fightRoom = (req, res) => {
		const room_id = req.params.roomId
		let user1 = {
			p1: req.body.p1_id,
			p1Pick: req.body.p1Pick,
		}

		let user2 = {
			p2: req.body.p2_id,
			p2Pick: req.body.p2Pick,
		}

		let result = []

		result.push({user1, user2})
    }

    getScore = (req, res) => {
        History.findAll()
			.then((history) => {
				res.send(history)
			})
			.catch((err) => console.log(err))
    }
	
	addScore = (req,res) => {
		History.update({
			user_id: req.body.user_id,
			score: req.body.score,
		}).then(() => {
			res.send("Updated")
		})
	}

	deleteScore = (req,res) => {
		History.destroy({
			where: {id: req.params.id},
		}).then(() => {
			res.send("Deleted");
		});
	}
}

module.exports = UserController
