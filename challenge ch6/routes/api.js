const { Router } = require('express')
const UserController = require("../controllers/api/UserController");
const AuthController = require("../controllers/api/AuthController")

const userController = new UserController()
const authController = new AuthController()
// const restrictApi = require("../middlewares/restrict-api");

const api = Router()

// auth
api.post("/login", authController.login)
api.post("/register", authController.register)

// api.use(restrictApi)

// user
api.get('/user', userController.getUser)
api.get('/user/:id', userController.getDetailUser)

// score user
api.post("/add-score", userController.addScore)
api.get("/score", userController.getScore)
api.delete("/score/:id", userController.deleteScore)

// game
api.post("/create-room", userController.createRoom)
api.post("/fight/room_id", userController.fightRoom)

module.exports = api