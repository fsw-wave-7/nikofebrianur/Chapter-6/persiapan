const express = require('express')
const morgan = require('morgan')
const path = require('path')
const {join} = require('path')
const api = require('./routes/api.js')
const web = require('./routes/web.js')

const app = express()
const port = 3000 

app.use(express.static(__dirname + '/public'))

app.set('view engine', 'ejs')

app.use(morgan('dev'))

app.use('/api', api)
app.use('/', web)

app.use(function(err, req, res, next) {
    res.status(500).json({
        status: 'fail',
        errors: err.message
    })
})

app.use(function(req,res, next) {
    res.render(join(__dirname, './views/404'))
})

app.listen(port, () => {
    console.log("Server ON dude!")
})