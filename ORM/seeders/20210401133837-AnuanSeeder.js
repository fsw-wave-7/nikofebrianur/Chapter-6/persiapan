'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('Anuans', [{
    name: "John Anoe",
    username: "johnanoe",
    age: 27,
    password: "anuansijohn",
    address: "Jalan Anu Gang Anu",
    phone: 62088800,
    createdAt: new Date,
    updatedAt: new Date
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Anuans', null, {});

  }
};
